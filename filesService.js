// requires...
const fs = require('fs');
const path = require('path');

// constants...
const dir = './files/';

function createFile(req, res, next) {
    const content = req.body.content;
    if (!req.body.filename) {
        res.status(400).send({
            message: "Please specify 'filename' parameter"
        });
    } else if (!content) {
        res.status(400).send({ message: "Please specify 'content' parameter" });
    } else {
        fs.writeFileSync(`${dir}${req.body.filename}`, content, 'utf-8');
        res.status(200).send({ message: 'File created successfully' });
    }
}

function getFiles(req, res, next) {
    // Your code to get all files.
    const files = [];
    fs.readdirSync('files').forEach((file) => {
        files.push(file);
    });

    res.status(200).send({
        message: 'Success',
        files: files
    });
}

const getFile = (req, res, next) => {
    // Your code to get all files.
    const filename = req.params.filename;
    if (fs.existsSync(`${dir}${filename}`)) {
        const data = fs.readFileSync(`${dir}${filename}`, {
            encoding: 'utf8',
            flag: 'r'
        });
        const extension = path.extname(`${dir}${filename}`).slice(1);
        const uploadedDate = fs.statSync(`${dir}${filename}`).mtime;
        console.log(req.params);
        res.status(200).send({
            message: 'Success',
            filename: filename,
            content: data,
            extension: extension,
            uploadedDate: uploadedDate
        });
    } else {
        res.status(400).send({
            message: `No file with '${filename}' filename found`
        });
    }
};

// Other functions - editFile, deleteFile

const editFile = (req, res, next) => {
    const content = req.body.content;
    const filename = req.params.filename;
    let message;

    if (fs.existsSync(`${dir}${filename}`)) {
        message = 'File updated successfully';
    } else {
        message = 'File created successfully';
    }
    fs.writeFileSync(`${dir}${filename}`, content, 'utf-8');
    res.status(200).send({ message: message });
};

const deleteFile = (req, res, next) => {
    const filename = req.params.filename;
    if (fs.existsSync(`${dir}${filename}`)) {
        fs.unlinkSync(`${dir}${filename}`);
        res.status(200).send({ message: 'File deleted successfully' });
    } else {
        res.send({ message: 'File does not exist' });
    }
};

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
    createFile,
    getFiles,
    getFile,
    editFile,
    deleteFile
};
